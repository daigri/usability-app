
import { Recomendation } from "../Interfaces/Recomendation";

export class TaskAPI {
    public static async getAll():Promise<Recomendation[]> {
        const resp = await fetch("http://localhost:3000/recomendation", {
            method:"GET"
        }).then(response =>  response.json());
    

        /*const data = resp;
        let respObject = JSON.parse(data);*/
        let recomendations = <Recomendation[]> resp;
        return recomendations;
    }
}