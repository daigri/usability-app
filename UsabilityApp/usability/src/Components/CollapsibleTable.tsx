import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import { Row, RowProps } from "./Row";
import Paper from "@mui/material/Paper";
import { TableCell, TableRow } from "@mui/material";
import { width } from "@mui/system";

export interface RowsWithId {
id:number,
data:RowProps,
};

export interface Cell {
content: string,
style?: React.CSSProperties
};

export interface ColapsibleTableProps {
  head: Cell[],
  rows: RowsWithId[],
};

export function CollapsibleTable(props: ColapsibleTableProps) {
    return (
      <TableContainer component={Paper}>
        <Table aria-label="collapsible table">
          <TableHead>
            <TableRow>
            <TableCell style={{ width: "32" }}/>
              {props.head.map((cell,index) => 
                <TableCell key={index} style={cell.style} align="left">{cell.content}</TableCell>
                )}
              <TableCell style={{ width: "32" }}/>
            </TableRow>
          </TableHead>
          <TableBody>
            {props.rows.map((row) => (
              <Row key={row.id} {...row.data}/>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    );
  }