import React from "react";
import Box from "@mui/material/Box";
import Collapse from "@mui/material/Collapse";
import IconButton from "@mui/material/IconButton";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import KeyboardArrowUpIcon from "@mui/icons-material/KeyboardArrowUp";

export default function CollapsedRow(props: {childrenRow:any, row:any} ) {
    const [openFunction, setOpenFunction] = React.useState(false);
  
    return (
      <React.Fragment>
        <TableRow key={props.childrenRow.name}>
          <TableCell width={32}>
            <IconButton
              aria-label="expand row"
              size="small"
              onClick={() => setOpenFunction(!openFunction)}
            >
              {openFunction ? (
                <KeyboardArrowUpIcon />
              ) : (
                <KeyboardArrowDownIcon />
              )}
            </IconButton>
          </TableCell>
          <TableCell component="th" scope="row">
            {props.childrenRow.name}
          </TableCell>
        </TableRow>
        <TableRow>
          <TableCell
            style={{ paddingBottom: 0, paddingTop: 0 }}
            colSpan={6}
          >
            <Collapse
              in={openFunction}
              timeout="auto"
              unmountOnExit
            >
              <Box margin={1}>
                <Table size="small" aria-label="purchases">
                  <TableHead>
                    <TableRow>
                      <TableCell></TableCell>
                      <TableCell>User</TableCell>
                      <TableCell>User</TableCell>
                      <TableCell>User</TableCell>
                      <TableCell>User</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {props.row.children.map((childrenRow:any) => (
                      <TableRow key={childrenRow.name}>
                        <TableCell component="th" scope="row">
                          {childrenRow.name}
                        </TableCell>
                        <TableCell>{childrenRow.user}</TableCell>
                        <TableCell>{childrenRow.user}</TableCell>
                        <TableCell>{childrenRow.user}</TableCell>
                        <TableCell>{childrenRow.user}</TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </Box>
            </Collapse>
          </TableCell>
        </TableRow>
      </React.Fragment>
    );
  };