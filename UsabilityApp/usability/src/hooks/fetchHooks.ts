import { useEffect, useState } from "react";
import { Area } from "../Interfaces/Area";
import { Criteria } from "../Interfaces/Criteria";
import { Problem } from "../Interfaces/Problem";
import { Recomendation } from "../Interfaces/Recomendation";
import { Rule } from "../Interfaces/Rule";

/*export const useFetch = <T,>(url: string) => {
    const [data, setData] = useState<T[]>([]);
    const [error, setError] = useState();
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        setLoading(true);
        fetch(url)
            .then(response => response.json())
            .then(setData)
            .catch(setError)
            .finally(() => setLoading(false));
    }, [url]);

    return { data, error, loading };
};*/

const useFetch = <T,>(url: string) => {
    const [data, setData] = useState<T[]>([]);
    const [isPending, setIsPending] = useState(true);
    const [error, setError] = useState(null);
  
    useEffect(() => {
      setTimeout(() => {
        fetch(url)
        .then(res => {
          if (!res.ok) { // error coming back from server
            throw Error('could not fetch the data for that resource');
          } 
          return res.json();
        })
        .then(data => {
          setIsPending(false);
          setData(data);
          setError(null);
        })
        .catch(err => {
          debugger;
          // auto catches network / connection error
          setIsPending(false);
          setError(err.message);
        })
      }, 1000);
    }, [url])
  
    return { data, isPending, error };
  }

  const useFetchSingle = <T,>(url: string) => {
    const [data, setData] = useState<T>();
    const [isPending, setIsPending] = useState(true);
    const [error, setError] = useState(null);
  
    useEffect(() => {
      setTimeout(() => {
        fetch(url)
        .then(res => {
          if (!res.ok) { // error coming back from server
            throw Error('could not fetch the data for that resource');
          } 
          return res.json();
        })
        .then(data => {
          setIsPending(false);
          setData(data);
          setError(null);
        })
        .catch(err => {
          debugger;
          // auto catches network / connection error
          setIsPending(false);
          setError(err.message);
        })
      }, 1000);
    }, [url])
  
    return { data, isPending, error };
  }


export const useRecomendations = () => {
    const [data, setData] = useState<Recomendation[]>([]);
    const [error, setError] = useState();
    const [loading, setLoading] = useState(false);

    useEffect(()=>{
        setLoading(true);
        fetch("http://localhost:3000/recomendation", {method:"GET"})
        .then(response =>  response.json())
        .then(resp => setData(resp))
        .catch(setError)
        .finally(() => setLoading(false));
      },[]);

      return {data, error, loading}
} 

export const useProblems =() => useFetch<Problem>("http://localhost:3000/problem");
export const useProblem =(id?:string) => useFetchSingle<Problem>(`http://localhost:3000/problem/${id}`);
export const useCriteria =() => useFetch<Criteria>("http://localhost:3000/criteria");
export const useArea =() => useFetch<Area>("http://localhost:3000/area");
export const useRule =() => useFetch<Rule>("http://localhost:3000/rule");

/*export const useProblems = () => {
    const [data, setData] = useState<Problem[]>([]);
    const [error, setError] = useState();
    const [loading, setLoading] = useState(false);
    const resp = (resp:any) => {
            return resp;
    };

    useEffect(()=>{
        setLoading(true);
        fetch("http://localhost:3000/problem", {method:"GET"})
        .then(response =>  response.json())
        .then (resp)
        .then(resp => setData(resp))
        .catch(setError)
        .finally(() => setLoading(false));
      },[]);

      return {data, error, loading}
} */