import { Box, Grid, TextField } from "@mui/material";
import { makeStyles } from "@mui/styles";
import React, { useEffect, useState } from "react";
import { Problem, ProblemForm } from "../Interfaces/Problem";
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import List from '@mui/material/List';
import ListItemText from '@mui/material/ListItemText';
import { useProblem } from "../hooks/fetchHooks";
import { useParams } from 'react-router';

const useStyle = makeStyles(theme => ({
    root:{
        '& .MuiFormControl-root': {
            width:'80%'
        }

    }
}));

export interface ProblemPageProps {
    problemId: number
}

export const ProblemPage:React.FC = (props) => {
    //const [problem,setProblem] = useState<ProblemForm>();
    const { id } = useParams();
    debugger;
    const {data} = useProblem(id);
    const classes = useStyle();
    const handleSubmit = () => {};


    return <>
    <form className={classes.root}>
        <Grid
            container
            spacing={0}
            direction="column"
            alignItems="center"
            justifyContent="center"
            style={{ minHeight: '50vh' }}
        >

            <Grid item xs={3}>
                    <TextField
                        disabled
                        id="code"
                        label="Code"
                        value={data?.code || ''}
                        InputProps={{
                            readOnly: true,
                          }}
                        variant="standard"
                    />
                    <TextField
                        disabled
                        id="name"
                        label="Name"
                        value={data?.name || ''}
                        InputProps={{
                            readOnly: true,
                          }}
                        variant="standard"
                    />
                    <TextField
                        id="description"
                        label="Description"
                        multiline
                        rows={4}
                        value={data?.description || ''}
                        InputProps={{
                            readOnly: true,
                          }}
                        variant="standard"
                    />
                    <Box
                        sx={{ width: '100%', height: 400, maxWidth: 360, bgcolor: 'background.paper' }}
                    >
                        <List>
                            {data?.criteriaRef.map(criteria => {
                                <ListItem disablePadding>
                                <ListItemButton>
                                    <ListItemText primary={criteria.name} />
                                </ListItemButton>
                            </ListItem>
                            })}
                        </List>
                    </Box>
                
            </Grid>

        </Grid>

    </form>
    </>;
}