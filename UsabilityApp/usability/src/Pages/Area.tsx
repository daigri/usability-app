import * as React from 'react';
import { useArea } from '../hooks/fetchHooks';
import { CollapsibleTable, Cell, RowsWithId } from '../Components/CollapsibleTable';
import { Criteria } from '../Interfaces/Criteria';
import { Button, CircularProgress, Grid, List, ListItem, ListItemText, TextField } from '@mui/material';
import AddCircleIcon from '@mui/icons-material/AddCircle';
import { Box } from '@mui/system';
import { Area } from '../Interfaces/Area';

const head: Cell[] = [
  {content: "Code", style:{width:"10%"}},
  {content: "Name", style:{width:"20%"}},
  {content: "Description", style:{width:"40%"}},
  {content: "Criteria"}
]
export const Areas = () =>{
  debugger;
  const areaList = useArea();

  const problemCellContent = (criteria: Criteria[]) => {
      switch (criteria.length) {
          case 0:
            return "";
          case 1:
              return criteria[0].name;
          default:
              return "Multiple";
      }
  }

  const collapsedRow = (area: Area) => {
    return <>
    {areaList.isPending ? <CircularProgress /> :
      
        <Grid
          container
          spacing={0}
          style={{ minHeight: '5vh' }}
        >
          <Grid item xs={3} container spacing={5} direction="column">
            <Grid item xs={3}>
              <TextField
                id="code"
                label="Code"
                value={area?.code || ''}
                InputProps={{
                  readOnly: true,
                }}
                variant="standard"
                style={{ width: '80%' }}
              />
            </Grid>
            <Grid item xs={3}>
              <TextField
                id="name"
                label="Name"
                value={area?.name || ''}
                InputProps={{
                  readOnly: true,
                }}
                variant="standard"
                style={{ width: '80%' }}
              />


            </Grid>
          </Grid>
          <Grid item xs={6}>
            <TextField
              id="description"
              label="Description"
              multiline
              rows={6}
              value={area?.description || ''}
              InputProps={{
                readOnly: true,
              }}
              variant="outlined"
              style={{ width: '80%' }}
            />
          </Grid>
          <Grid item xs={3}>
            <Box
              sx={{ width: '100%', height: 200, maxWidth: 360, bgcolor: 'background.paper' }}
            >
              <List>
                <ListItem></ListItem>
                {area.criteriaRef.map((criteria, index) => {
                  <ListItem button key={index}>
                    <ListItemText primary={criteria.name} />
                  </ListItem>
                })}
              </List>
            </Box>
          </Grid>

        </Grid>
    }
  </>
  }

  const rows: RowsWithId[] = areaList.data.map((area): RowsWithId => {
    return {
      id: area.id,
      data: {
        colapsable: collapsedRow(area),
        data: [
          { content: area.code },
          { content: area.name },
          { content: area.description },
          { content: problemCellContent(area.criteriaRef)}
        ]
      }
    }
  });

  return <>
  <Box display="flex" justifyContent="flex-end"><Button variant="outlined" startIcon={< AddCircleIcon/>}>Create</Button></Box>
  {areaList.isPending? <CircularProgress />:<CollapsibleTable head={head} rows={rows} />}
  </>
}
