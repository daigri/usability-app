import * as React from 'react';
import { useRecomendations} from '../hooks/fetchHooks';
import { CollapsibleTable, Cell, RowsWithId } from '../Components/CollapsibleTable';
import { Button, CircularProgress, Grid, List, ListItem, ListItemText, TextField } from '@mui/material';
import AddCircleIcon from '@mui/icons-material/AddCircle';
import { Box } from '@mui/system';
import { Problem } from '../Interfaces/Problem';
import { Recomendation } from '../Interfaces/Recomendation';

const head: Cell[] = [
  {content: "Code", style:{width:"10%"}},
  {content: "Recomendation"},
  {content: "Problem"}
]
export const Recomendations = () =>{
  const recomendations = useRecomendations();
  debugger;

  const problemCellContent = (problem: Problem[]) => {
    return problem.length > 1? "Multiple" : problem[0].name;
  }

  const collapsedRow = (recomendation: Recomendation) => {
    return  <>
      {recomendations.loading ? <CircularProgress /> :
        
          <Grid
            container
            spacing={5}
            style={{ minHeight: '5vh' }}
            direction="column"
          >
            <Grid item xs={3} container spacing={5}>
              <Grid item xs={3}>
                <TextField
                  id="code"
                  label="Code"
                  value={recomendation?.code || ''}
                  InputProps={{
                    readOnly: true,
                  }}
                  variant="standard"
                  style={{ width: '80%' }}
                />
              </Grid>
             
            </Grid>
            <Grid item xs={3} container spacing={0}>
            <Grid item xs={6}>
              <TextField
                id="recomendation"
                label="Recomendation"
                multiline
                rows={6}
                value={recomendation?.recomendation || ''}
                InputProps={{
                  readOnly: true,
                }}
                variant="outlined"
                style={{ width: '90%' }}
              />
            </Grid>
            
            <Grid item xs={4}>
                <TextField
                  id="comment"
                  label="Comment"
                  multiline
                  rows={6}
                  value={recomendation?.comment || ''}
                  InputProps={{
                    readOnly: true,
                  }}
                  variant="outlined"
                  style={{ width: '90%' }}
                />
              </Grid>

              <Grid item xs={2}>
              <Box
                sx={{ width: '100%', height: 200, maxWidth: 360, bgcolor: 'background.paper' }}
              >
                <List>
                  <ListItem></ListItem>
                  {recomendation.problemRef.map((problem, index) => {
                    <ListItem button key={index}>
                      <ListItemText primary={problem.name} />
                    </ListItem>
                  })}
                </List>
              </Box>
            </Grid>

            </Grid>
            

          </Grid>
      }
    </>
  }

  const rows: RowsWithId[] = recomendations.data.map((recomendation): RowsWithId => {
    return {
      id: recomendation.id,
      data: {
        colapsable: collapsedRow(recomendation),
        data: [
          { content: recomendation.code },
          { content: recomendation.recomendation },
          { content: recomendation.problemRef? problemCellContent(recomendation.problemRef) : ''}
        ]
      }
    }
  });

  return <>
  <Box display="flex" justifyContent="flex-end"><Button variant="outlined" startIcon={< AddCircleIcon/>}>Create</Button></Box>
  {recomendations.loading? <CircularProgress />:<CollapsibleTable head={head} rows={rows} />}
  </>
}
