import * as React from 'react';
import { useCriteria } from '../hooks/fetchHooks';
import { Criteria as CriteriaInterface } from '../Interfaces/Criteria';
import { CollapsibleTable, Cell, RowsWithId } from '../Components/CollapsibleTable';
import { Problem } from '../Interfaces/Problem';
import { Button, CircularProgress, Grid, List, ListItem, ListItemText, TextField } from '@mui/material';
import AddCircleIcon from '@mui/icons-material/AddCircle';
import { Box } from '@mui/system';
import { Area } from '../Interfaces/Area';

const head: Cell[] = [
  {content: "Code", style:{width:"10%"}},
  {content: "Name", style:{width:"20%"}},
  {content: "Description", style:{width:"35%"}},
  {content: "Problem"},
  {content: "Area of use"}

]
export const Criteria = () =>{
  debugger;
  const criteriaList = useCriteria();

  const problemCellContent = (problem: Problem[]) => {
    switch (problem.length) {
        case 0:
          return "";
        case 1:
            return problem[0].name;
        default:
            return "Multiple";
    }
  }

  const aouCellContent = (aou: Area[]) => {
    switch (aou.length) {
        case 0:
          return "";
        case 1:
            return aou[0].name;
        default:
            return "Multiple";
    }
  }

  const collapsedRow = (criteria: CriteriaInterface) => {
    return  <>
    {criteriaList.isPending ? <CircularProgress /> :
        <Grid container spacing={5} direction="column">
          <Grid
            item xs={4}
            container
            spacing={5}
            style={{ minHeight: '5vh' }}
          >
            <Grid item xs={4} container spacing={5} direction="column">
              <Grid item xs={4}>
                <TextField
                  id="code"
                  label="Code"
                  value={criteria?.code || ''}
                  InputProps={{
                    readOnly: true,
                  }}
                  variant="standard"
                  style={{ width: '90%' }}
                />
              </Grid>
              <Grid item xs={6}>
                <TextField
                  id="name"
                  label="Name"
                  value={criteria?.name || ''}
                  InputProps={{
                    readOnly: true,
                  }}
                  variant="standard"
                  style={{ width: '90%' }}
                />


              </Grid>
            </Grid>
            <Grid item xs={8}>
              <TextField
                id="description"
                label="Description"
                multiline
                rows={6}
                value={criteria?.description || ''}
                InputProps={{
                  readOnly: true,
                }}
                variant="outlined"
                style={{ width: '90%' }}
              />
            </Grid>
            

          </Grid>
          <Grid
            item xs={4}
            container
            spacing={0}
            style={{ minHeight: '5vh' }}
          >
            <Grid item xs={3}>
              <Box
                sx={{ width: '100%', height: 200, maxWidth: 360, bgcolor: 'background.paper' }}
              >
                <List>
                  <ListItem></ListItem>
                  {criteria.problemRef.map((problem, index) => {
                    <ListItem button key={index}>
                      <ListItemText primary={problem.name} />
                    </ListItem>
                  })}
                </List>
              </Box>
            </Grid>

            <Grid item xs={3}>
              <Box
                sx={{ width: '100%', height: 200, maxWidth: 360, bgcolor: 'background.paper' }}
              >
                <List>
                  <ListItem></ListItem>
                  {criteria.areOfUseRef.map((aou, index) => {
                    <ListItem button key={index}>
                      <ListItemText primary={aou.name} />
                    </ListItem>
                  })}
                </List>
              </Box>
            </Grid>
            </Grid>
        </Grid>
      
    }
  </>
  }

  const rows: RowsWithId[] = criteriaList.data.map((criteria): RowsWithId => {
    return {
      id: criteria.id,
      data: {
        colapsable: collapsedRow(criteria),
        data: [
          { content: criteria.code },
          { content: criteria.name },
          { content: criteria.description },
          { content: problemCellContent(criteria.problemRef) },
          { content: aouCellContent(criteria.areOfUseRef) }
        ]
      }
    }
  });

return <>
<Box display="flex" justifyContent="flex-end"><Button variant="outlined" startIcon={< AddCircleIcon/>}>Create</Button></Box>
{criteriaList.isPending? <CircularProgress />:<CollapsibleTable head={head} rows={rows} />}
</>

}
