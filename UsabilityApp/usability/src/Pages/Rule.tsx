import * as React from 'react';
import { useProblems } from '../hooks/fetchHooks';
import { CollapsibleTable, Cell, RowsWithId } from '../Components/CollapsibleTable';
import { Criteria } from '../Interfaces/Criteria';
import { Button, CircularProgress } from '@mui/material';
import AddCircleIcon from '@mui/icons-material/AddCircle';
import { Box } from '@mui/system';

const head: Cell[] = [
  {content: "Code", style:{width:"10%"}},
  {content: "Name"},
  {content: "Criteria"}
]
export const Problems = () =>{
  debugger;
  const problems = useProblems();

  const problemCellContent = (criteria: Criteria[]) => {
    switch (criteria.length) {
        case 0:
          return "";
        case 1:
            return criteria[0].name;
        default:
            return "Multiple";
    }
  }

  const collapsedRow = () => {
    return <div></div>
  }

  const rows: RowsWithId[] = problems.data.map((problem): RowsWithId => {
    return {
      id: problem.id,
      data: {
        colapsable: collapsedRow(),
        data: [
          { content: problem.code },
          { content: problem.name },
          { content: problemCellContent(problem.criteriaRef) }
        ]
      }
    }
  });

  return <>
  <Box display="flex" justifyContent="flex-end"><Button variant="outlined" startIcon={< AddCircleIcon/>}>Create</Button></Box>
  {problems.isPending? <CircularProgress />:<CollapsibleTable head={head} rows={rows} />}
  </>
}
