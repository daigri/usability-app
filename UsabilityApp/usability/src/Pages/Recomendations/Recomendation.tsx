import { ReactJSXElement } from '@emotion/react/types/jsx-namespace';
import { DataGrid, GridColDef } from '@mui/x-data-grid';
import { useRecomendations } from '../../hooks/fetchHooks';

const columns: GridColDef[] = [
  { field: 'code', headerName: 'Code', width: 130 },
  { field: 'recomendation', headerName: 'Recomendation', width: 600 },
  { field: 'problemRef', headerName: 'Problem', width: 400, sortable: false ,valueGetter: (params) => { return params.value[0].name} },
  { field: 'comment', headerName: 'Comment', flex: 1  },
];

export default function RecomendationGrid() {
  debugger;
  const recomendations = useRecomendations();

  return (
    <div style={{ display: 'flex', height: '100%' }}>
    <div style={{ flexGrow: 1 }}>
      <DataGrid columns={columns} rows={recomendations.data} />
    </div>
  </div>
  );
}

