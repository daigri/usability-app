import * as React from 'react';
import { useProblems } from '../hooks/fetchHooks';
import { CollapsibleTable, Cell, RowsWithId } from '../Components/CollapsibleTable';
import { Criteria } from '../Interfaces/Criteria';
import { Button, CircularProgress, Container, Grid, TextField } from '@mui/material';
import AddCircleIcon from '@mui/icons-material/AddCircle';
import { Box } from '@mui/system';
import ListItem from '@mui/material/ListItem';
import List from '@mui/material/List';
import ListItemText from '@mui/material/ListItemText';
import { Problem } from '../Interfaces/Problem';

const head: Cell[] = [
  {content: "Code", style:{width:"10%"}},
  {content: "Name"},
  {content: "Description", style:{width:"40%"}},
  {content: "Criteria"}
]
export const Problems = () =>{
  const problems = useProblems();

  const problemCellContent = (criteria: Criteria[]) => {
    return criteria.length > 1? "Multiple" : criteria[0].name;
  }

  const collapsedRow = (problem: Problem): JSX.Element => {
    return <>
      {problems.isPending ? <CircularProgress /> :
        
          <Grid
            container
            spacing={0}
            style={{ minHeight: '5vh' }}
          >
            <Grid item xs={3} container spacing={5} direction="column">
              <Grid item xs={3}>
                <TextField
                  id="code"
                  label="Code"
                  value={problem?.code || ''}
                  InputProps={{
                    readOnly: true,
                  }}
                  variant="standard"
                  style={{ width: '80%' }}
                />
              </Grid>
              <Grid item xs={3}>
                <TextField
                  id="name"
                  label="Name"
                  value={problem?.name || ''}
                  InputProps={{
                    readOnly: true,
                  }}
                  variant="standard"
                  style={{ width: '80%' }}
                />


              </Grid>
            </Grid>
            <Grid item xs={6}>
              <TextField
                id="description"
                label="Description"
                multiline
                rows={6}
                value={problem?.description || ''}
                InputProps={{
                  readOnly: true,
                }}
                variant="outlined"
                style={{ width: '80%' }}
              />
            </Grid>
            <Grid item xs={3}>
              <Box
                sx={{ width: '100%', height: 200, maxWidth: 360, bgcolor: 'background.paper' }}
              >
                <List>
                  <ListItem></ListItem>
                  {problem.criteriaRef.map((criteria, index) => {
                    <ListItem button key={index}>
                      <ListItemText primary={criteria.name} />
                    </ListItem>
                  })}
                </List>
              </Box>
            </Grid>

          </Grid>
      }
    </>
  }

  const rows: RowsWithId[] = problems.data.map((problem:Problem): RowsWithId => {
    return {
      id: problem.id,
      data: {
        colapsable: collapsedRow(problem),
        data: [
          { content: problem.code },
          { content: problem.name },
          { content: problem.description },
          { content: problemCellContent(problem.criteriaRef) }
        ]
      }
    }
  });

  return <>
  <Box display="flex" justifyContent="flex-end"><Button variant="outlined" startIcon={< AddCircleIcon/>}>Create</Button></Box>
  {problems.isPending? <CircularProgress />:<CollapsibleTable head={head} rows={rows} />}
  </>
}
