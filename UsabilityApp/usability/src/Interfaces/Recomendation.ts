import { Problem } from "./Problem";
import { Rule } from "./Rule";

export interface Recomendation {
    id: number;
    recomendation: string;
    code: string;
    comment: string;
    problemRef: Problem[];
    ruleRef: Rule[];
}