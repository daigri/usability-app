import { Area } from "./Area";
import { Problem } from "./Problem";
import { Rule } from "./Rule";

export interface Criteria {
    id: number;
    name: string;
    code: string;
    description: string;
    rulesRef: Rule[];
    areOfUseRef: Area[];
    problemRef: Problem[];
}