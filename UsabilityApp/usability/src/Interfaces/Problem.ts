import { Criteria } from "./Criteria";
import { Recomendation } from "./Recomendation";

export interface Problem {
    id:number;
    name: string;
    code: string;
    description: string;
    criteriaRef: Criteria[];
    recomendationsRef: Recomendation[];
}

export interface ProblemForm {
    id:number;
    name: string;
    code: string;
    description: string;
    criteriaIds: number[];
    recomendationIds: number[];
}