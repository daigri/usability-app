import { Criteria } from "./Criteria";
import { Rule } from "./Rule";

export interface Area {
    id: number;
    name: string;
    code: string;
    description: string;
    ruleRef: Rule[];
    criteriaRef: Criteria[];
}