import React from 'react';
import './App.css';
import {BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import { CssBaseline } from '@mui/material';
import {Recomendations} from './Pages/Recomendation';
import { Problems } from './Pages/Problems';
import { Areas } from './Pages/Area';
import { Criteria } from './Pages/Criteria';
import {MenuDrawer} from './Components/MenuDrawer';
import { ProblemPage } from './Pages/ProblemPage';

function App() {
  return (
    <>
      
    < CssBaseline /> 
        <div className="App">
            <Router>
              <MenuDrawer>

              <Routes>
                  <Route path='/' element={<Recomendations/>}/>
                  <Route path='/problem' element={<Problems/>} />
                  <Route path='/problem/:id' element={<ProblemPage/>} />
                  <Route path='/area' element={<Areas />} />
                  <Route path='/criteria' element={<Criteria/>} />
                  <Route path='/test' element={<ProblemPage/>} />
              </Routes>
              </MenuDrawer>

      
            </Router>   
        </div>
    </>
  );
}

export default App;
