import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AreaOfUse } from 'src/entity/AreaOfUse';
import { AreaController } from './area.controller';
import { AreaService } from './area.service';

@Module({
    controllers: [AreaController],
    providers: [AreaService],
    imports: [TypeOrmModule.forFeature([AreaOfUse])]
})
export class AreaModule {}
