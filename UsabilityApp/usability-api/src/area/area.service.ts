import { HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { plainToClass } from 'class-transformer';
import { CreateAreaDTO as CreateAreaDTO, ResponseAreaDTO } from 'src/dto/area.dto';
import { AreaOfUse } from 'src/entity/AreaOfUse';
import { Recomendation } from 'src/entity/Recomendation';
import { Repository } from 'typeorm';
@Injectable()
export class AreaService {
    constructor(@InjectRepository(AreaOfUse) private AreaOfUseRepository:Repository<Recomendation>){}

    public async create (dto: CreateAreaDTO): Promise<ResponseAreaDTO>{
       // var recomendationJson = instanceToPlain(CreateRecomendationDTO, { excludeExtraneousValues: true });
        var area = plainToClass(AreaOfUse, dto);
        this.AreaOfUseRepository.save(area);
        return plainToClass(ResponseAreaDTO,area);
    }

    public async get (id: number): Promise<ResponseAreaDTO>{
        var area = this.AreaOfUseRepository.findOne(id,{relations:['ruleRef','criteriaRef']});
        var resp = plainToClass(ResponseAreaDTO, area);
        return resp;
    }

    public async getAll (): Promise<ResponseAreaDTO[]>{
        var area = await this.AreaOfUseRepository.find({relations:['ruleRef','criteriaRef']})
        var resp = area.map(x => plainToClass(ResponseAreaDTO,x));
        return  resp;
    }

    public async delete (id: number): Promise<HttpStatus>{
        var area = await this.AreaOfUseRepository.findOne(id);
        var resp = this.AreaOfUseRepository.remove(area);
        return HttpStatus.ACCEPTED;
    }

    /*public async update (dto: UpdateRecomendationDTO): Promise<ResponseRecomendationDTO>{
        var recomendation = this.recomendationRepository.findOne(dto.id);
        recomendation
        return plainToClass(ResponseRecomendationDTO,recomendation);
    }*/
}
