import { CreateAreaDTO, ResponseAreaDTO } from 'src/dto/area.dto';
import { AreaService } from './area.service';
import { Body, Controller, Delete, Get, Param, Post } from "@nestjs/common";

@Controller('area')
export class AreaController {
    
    constructor (private readonly areaService: AreaService){};

    @Get()
    async getAll():Promise<ResponseAreaDTO[]> {
        return await this.areaService.getAll();
    }

    @Get('/:id')
    async one(@Param('id') id: number) {
        return await this.areaService.get(id);
    }

    @Post()
    async save(@Body() area: CreateAreaDTO) {
        return await this.areaService.create(area);
    }

    @Delete('/:id')
    async remove(@Param('id') id: number) {
       return await this.areaService.delete(id);
    }
}
