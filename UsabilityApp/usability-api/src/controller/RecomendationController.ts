import { CreateRecomendationDTO, ResponseRecomendationDTO } from "src/dto/recomendation.dto";
import { RecomendationService } from "src/recomendation/recomendation.service";
import { Body, Controller, Delete, Get, Param, Post } from "@nestjs/common";

@Controller('recomendation')
export class RecomendationController {

    constructor (private readonly recomendationService: RecomendationService){};

    @Get()
    async getAll():Promise<ResponseRecomendationDTO[]> {
        return await this.recomendationService.getAll();
    }

    @Get('/:id')
    async one(@Param('id') id: number) {
        return await this.recomendationService.get(id);
    }

    @Post()
    async save(@Body() recomendation: CreateRecomendationDTO) {
        return await this.recomendationService.create(recomendation);
    }

    @Delete(':id')
    async remove(@Param('id') id: number) {
       return await this.recomendationService.delete(id);
    }

}