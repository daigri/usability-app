import { Body, Controller, Delete, Get, Param, Post } from "@nestjs/common";
import { CreateCriteriaDTO, ResponseCriteriaDTO } from "src/dto/criteria.dto";
import { CriteriaService } from "./criteria.service";

@Controller('criteria')
export class CriteriaController {
    constructor (private readonly criteriaService: CriteriaService){};

    @Get()
    async getAll():Promise<ResponseCriteriaDTO[]> {
        return await this.criteriaService.getAll();
    }

    @Get('/:id')
    async one(@Param('id') id: number) {
        return await this.criteriaService.get(id);
    }

    @Post()
    async save(@Body() critera: CreateCriteriaDTO) {
        return await this.criteriaService.create(critera);
    }

    @Delete('/:id')
    async remove(@Param('id') id: number) {
       return await this.criteriaService.delete(id);
    }
}
