import { HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { plainToClass } from 'class-transformer';
import { Repository } from 'typeorm';
import { Criteria } from 'src/entity/Criteria';
import { CreateCriteriaDTO, ResponseCriteriaDTO } from 'src/dto/criteria.dto';

@Injectable()
export class CriteriaService {
    constructor(@InjectRepository(Criteria) private criteriaRepository:Repository<Criteria>){}

    public async create (dto: CreateCriteriaDTO): Promise<ResponseCriteriaDTO>{
       // var recomendationJson = instanceToPlain(CreateRecomendationDTO, { excludeExtraneousValues: true });
        var criteria = plainToClass(Criteria, dto);
        this.criteriaRepository.save(criteria);
        return plainToClass(ResponseCriteriaDTO,criteria);
    }

    public async get (id: number): Promise<ResponseCriteriaDTO>{
        var criteria = this.criteriaRepository.findOne(id,{relations:['ruleRef','areOfUseRef','problemRef']});
        var resp = plainToClass(ResponseCriteriaDTO, criteria);
        return resp;
    }

    public async getAll (): Promise<ResponseCriteriaDTO[]>{
        var criteria = await this.criteriaRepository.find({relations:['ruleRef','areOfUseRef','problemRef']})
        var resp = criteria.map(x => plainToClass(ResponseCriteriaDTO,x));
        return  resp;
    }

    public async delete (id: number): Promise<HttpStatus>{
        var criteria = await this.criteriaRepository.findOne(id);
        var resp = this.criteriaRepository.remove(criteria);
        return HttpStatus.ACCEPTED;
    }

    /*public async update (dto: UpdateRecomendationDTO): Promise<ResponseRecomendationDTO>{
        var recomendation = this.recomendationRepository.findOne(dto.id);
        recomendation
        return plainToClass(ResponseRecomendationDTO,recomendation);
    }*/
}
