import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Criteria } from 'src/entity/Criteria';
import { CriteriaController } from './criteria.controller';
import { CriteriaService } from './criteria.service';

@Module({
    controllers: [CriteriaController],
    providers: [CriteriaService],
    imports: [TypeOrmModule.forFeature([Criteria])]
})
export class CriteriaModule {}
