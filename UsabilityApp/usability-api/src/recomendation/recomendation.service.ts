import { HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { plainToClass } from 'class-transformer';
import { CreateRecomendationDTO, ResponseRecomendationDTO } from 'src/dto/recomendation.dto';
import { Problem } from 'src/entity/Problem';
import { Recomendation } from 'src/entity/Recomendation';
import { Rule } from 'src/entity/Rule';
import { Repository } from 'typeorm';

@Injectable()
export class RecomendationService {
    constructor(
        @InjectRepository(Recomendation) private recomendationRepository:Repository<Recomendation>,
        @InjectRepository(Problem) private problemRepository:Repository<Problem>,
        @InjectRepository(Rule) private ruleRepository:Repository<Rule>
        ){}


    public async create (dto: CreateRecomendationDTO): Promise<ResponseRecomendationDTO>{
       // var recomendationJson = instanceToPlain(CreateRecomendationDTO, { excludeExtraneousValues: true });
        var recomendation = new Recomendation();
        recomendation.code = dto.code;
        recomendation.comment = dto.code;
        recomendation.recomendation = dto.recomendation;
        dto.problemIds ? recomendation.problemRef = await this.problemRepository.findByIds(dto.problemIds):null;
        dto.ruleIds? recomendation.ruleRef = await this.ruleRepository.findByIds(dto.ruleIds):null;
        await this.recomendationRepository.save(recomendation);
        return plainToClass(ResponseRecomendationDTO,recomendation);
    }

    public async get (findId: number): Promise<ResponseRecomendationDTO>{
        var recomendation = await this.recomendationRepository.createQueryBuilder("recomendation")
        .where("recomendation.id = :id", {id: findId})
        .leftJoinAndSelect("recomendation.problemRef", "problem")
        .leftJoinAndSelect("recomendation.ruleRef", "rule")
        .leftJoinAndSelect("problem.criteriaRef","criteria")
        .leftJoinAndSelect("criteria.areOfUseRef", "aou")
        .getOne();
        var resp = plainToClass(ResponseRecomendationDTO, recomendation);
        return resp;
    }

    public async getAll (): Promise<ResponseRecomendationDTO[]>{       
        var a = await this.recomendationRepository.createQueryBuilder("recomendation")
        .leftJoinAndSelect("recomendation.problemRef", "problem")
        .leftJoinAndSelect("recomendation.ruleRef", "rule")
        .leftJoinAndSelect("problem.criteriaRef","criteria")
        .leftJoinAndSelect("criteria.areOfUseRef", "aou").getQueryAndParameters();
        
        
        var recomendations = await this.recomendationRepository.createQueryBuilder("recomendation")
        .leftJoinAndSelect("recomendation.problemRef", "problem")
        .leftJoinAndSelect("recomendation.ruleRef", "rule")
        .leftJoinAndSelect("problem.criteriaRef","criteria")
        .leftJoinAndSelect("criteria.areOfUseRef", "aou")
        .getMany();
        var resp = recomendations.map(x => plainToClass(ResponseRecomendationDTO,x));
        return  resp;
    }

    public async delete (id: number): Promise<HttpStatus>{
        var recomendation = await this.recomendationRepository.findOne(id);
        var resp = this.recomendationRepository.remove(recomendation);
        return HttpStatus.ACCEPTED;
    }

    /*public async update (dto: UpdateRecomendationDTO): Promise<ResponseRecomendationDTO>{
        var recomendation = this.recomendationRepository.findOne(dto.id);
        recomendation
        return plainToClass(ResponseRecomendationDTO,recomendation);
    }*/
}
