import { CreateRecomendationDTO, ResponseRecomendationDTO } from "src/dto/recomendation.dto";
import { RecomendationService } from "src/recomendation/recomendation.service";
import { Body, Controller, Delete, Get, Param, Post } from "@nestjs/common";

@Controller('recomendation')
export class RecomendationController {

    constructor (private readonly recomendationService: RecomendationService){};

    @Get()
    async getAll():Promise<ResponseRecomendationDTO[]> {
        var resp =  await this.recomendationService.getAll();
        return resp;
    }

    @Get('/:id')
    async one(@Param('id') id: number) {
        var resp = await this.recomendationService.get(id);
        return resp;
    }

    @Post()
    async save(@Body() recomendation: CreateRecomendationDTO) {
        return await this.recomendationService.create(recomendation);
    }

    @Delete('/:id')
    async remove(@Param('id') id: number) {
       return await this.recomendationService.delete(id);
    }

}