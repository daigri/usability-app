import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Problem } from 'src/entity/Problem';
import { Recomendation } from 'src/entity/Recomendation';
import { Rule } from 'src/entity/Rule';
import { RecomendationController } from './recomendation.controller';
import { RecomendationService } from './recomendation.service';

@Module({
  controllers: [RecomendationController],
  providers: [RecomendationService],
  imports: [TypeOrmModule.forFeature([Recomendation,Rule,Problem])]
})
export class RecomendationModule {}
