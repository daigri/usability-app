import { Problem } from 'src/entity/Problem';
import { HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { plainToClass } from 'class-transformer';
import { Repository } from 'typeorm';
import { CreateProblemDTO, ResponseProblemDTO } from 'src/dto/problem.dto';

@Injectable()
export class ProblemService {
    constructor(@InjectRepository(Problem) private problemRepository:Repository<Problem>){}

    public async create (dto: CreateProblemDTO): Promise<ResponseProblemDTO>{
       // var recomendationJson = instanceToPlain(CreateRecomendationDTO, { excludeExtraneousValues: true });
        var problem = plainToClass(Problem, dto);
        this.problemRepository.save(problem);
        return plainToClass(ResponseProblemDTO,problem);
    }

    public async get (id: number): Promise<ResponseProblemDTO>{
        var problem = this.problemRepository.findOne(id,{relations:['recomendationRef','criteriaRef']});
        var resp = plainToClass(ResponseProblemDTO, problem);
        return resp;
    }

    public async getAll (): Promise<ResponseProblemDTO[]>{
        var problem = await this.problemRepository.find({relations:['recomendationRef','criteriaRef']})
        var resp = problem.map(x => plainToClass(ResponseProblemDTO,x));
        return  resp;
    }


    public async delete (id: number): Promise<HttpStatus>{
        var problem = await this.problemRepository.findOne(id);
        var resp = this.problemRepository.remove(problem);
        return HttpStatus.ACCEPTED;
    }

    /*public async update (dto: UpdateRecomendationDTO): Promise<ResponseRecomendationDTO>{
        var recomendation = this.recomendationRepository.findOne(dto.id);
        recomendation
        return plainToClass(ResponseRecomendationDTO,recomendation);
    }*/
}
