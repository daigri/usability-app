import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Problem } from 'src/entity/Problem';
import { ProblemController } from './problem.controller';
import { ProblemService } from './problem.service';

@Module({
    controllers: [ProblemController],
    providers: [ProblemService],
    imports: [TypeOrmModule.forFeature([Problem])]
})
export class ProblemModule {}
