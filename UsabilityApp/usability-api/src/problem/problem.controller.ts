import { Body, Controller, Delete, Get, Param, Post } from "@nestjs/common";
import { CreateProblemDTO, ProblemDTO, ResponseProblemDTO } from "src/dto/problem.dto";
import { ProblemService } from "./problem.service";

@Controller('problem')
export class ProblemController {

    constructor (private readonly problemService: ProblemService){};

    @Get()
    async getAll():Promise<ResponseProblemDTO[]> {
        return await this.problemService.getAll();
    }

    @Get('/:id')
    async one(@Param('id') id: number) {
        return await this.problemService.get(id);
    }

    @Post()
    async save(@Body() problem: CreateProblemDTO) {
        return await this.problemService.create(problem);
    }

    @Delete('/:id')
    async remove(@Param('id') id: number) {
       return await this.problemService.delete(id);
    }
}
