import {Entity, PrimaryGeneratedColumn, Column, OneToMany, ManyToOne} from "typeorm";
import { Rule } from "./Rule";
import { UnitOfMeasurement } from "./UnitOfMeasurement";

@Entity("Metric")
export class Metric {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    code: string;

    @Column()
    description: string;

    @OneToMany(() => Rule, rule => rule.metricRef)
    ruleRef: Rule[];

    @ManyToOne(() => UnitOfMeasurement, uom => uom.metricRef)
    unitOfMeasurementRef: UnitOfMeasurement;
}