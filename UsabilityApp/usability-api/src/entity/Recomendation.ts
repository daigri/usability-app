import {Entity, PrimaryGeneratedColumn, Column, ManyToMany} from "typeorm";
import { Problem } from "./Problem";
import { Rule } from "./Rule";

@Entity("Recomendation")
export class Recomendation {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    recomendation: string;

    @Column({ unique: true })
    code: string;

    @Column()
    comment: string;

    @ManyToMany(() => Rule, rule => rule.recomendationRef)
    ruleRef: Rule[];

    @ManyToMany(() => Problem, problem => problem.recomendationRef)
    problemRef: Problem[];

}