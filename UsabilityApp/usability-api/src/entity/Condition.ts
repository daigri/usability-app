import {Entity, PrimaryGeneratedColumn, Column, ManyToOne} from "typeorm";
import { Rule } from "./Rule";

@Entity("Condition")
export class Condition {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    condition: string;

    @Column()
    order: number;

    @Column()
    operator: string;

    @ManyToOne(() => Rule, rule => rule.conditionRef)
    ruleRef: Rule;

}