import {Entity, PrimaryGeneratedColumn, Column, ManyToMany, JoinTable} from "typeorm";
import { Criteria } from "./Criteria";
import { Rule } from "./Rule";

@Entity("AreaOfUse")
export class AreaOfUse {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    code: string;

    @Column()
    description: string;
    
    @ManyToMany(() => Rule, rule => rule.areOfUseRef)
    ruleRef: Rule[];
    
    @ManyToMany(() => Criteria, criteria => criteria.areOfUseRef)
    @JoinTable()
    criteriaRef: Criteria[];
}