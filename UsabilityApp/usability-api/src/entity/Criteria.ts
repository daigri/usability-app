import {Entity, PrimaryGeneratedColumn, Column, ManyToMany} from "typeorm";
import { AreaOfUse } from "./AreaOfUse";
import { Problem } from "./Problem";
import { Rule } from "./Rule";

@Entity("Criteria")
export class Criteria {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column({ unique: true })
    code: string;

    @Column()
    description: string;
    
    @ManyToMany(() => Problem, problem => problem.criteriaRef)
    problemRef: Problem[];
    
    @ManyToMany(() => AreaOfUse, aou => aou.criteriaRef)
    areOfUseRef: AreaOfUse[];

    @ManyToMany(() => Rule, rule => rule.criteriaRef)
    ruleRef: Rule[];
}