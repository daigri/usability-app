import {Entity, PrimaryGeneratedColumn, Column, OneToMany} from "typeorm";
import { Metric } from "./Metric";

@Entity("UnitOfMeasurement")
export class UnitOfMeasurement {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    symbol: string;

    @OneToMany(() => Metric, metric => metric.unitOfMeasurementRef)
    metricRef: Metric[];

}