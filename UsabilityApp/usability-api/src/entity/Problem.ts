import {Entity, PrimaryGeneratedColumn, Column, ManyToMany, JoinTable} from "typeorm";
import { Criteria } from "./Criteria";
import { Recomendation } from "./Recomendation";

@Entity("Problem")
export class Problem {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({ unique: true })
    name: string;

    @Column()
    description: string;

    @Column({ unique: true })
    code: string;

    @ManyToMany(() => Recomendation, recomendation => recomendation.problemRef)
    @JoinTable()
    recomendationRef: Recomendation[]

    @ManyToMany(() => Criteria, criteria => criteria.problemRef)
    @JoinTable()
    criteriaRef: Criteria[]

}