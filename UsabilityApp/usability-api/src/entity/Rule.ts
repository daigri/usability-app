import {Entity, PrimaryGeneratedColumn, Column, OneToMany, ManyToMany, JoinTable, ManyToOne} from "typeorm";
import { AreaOfUse } from "./AreaOfUse";
import { Condition } from "./Condition";
import { Criteria } from "./Criteria";
import { Metric } from "./Metric";
import { Recomendation } from "./Recomendation";

@Entity("Rule")
export class Rule {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    code: string;

    @Column({ type: "timestamptz", default: "now()" })
    createdAt: Date = new Date();

    @ManyToOne(() => Metric, metric => metric.ruleRef)
    metricRef: Metric;

    @OneToMany(() => Condition, condition => condition.ruleRef)
    conditionRef: Condition[];

    @ManyToMany(() => Recomendation, recomendation => recomendation.ruleRef)
    @JoinTable()
    recomendationRef: Recomendation[]

    @ManyToMany(() => AreaOfUse, aou => aou.ruleRef)
    @JoinTable()
    areOfUseRef: AreaOfUse[]

    @ManyToMany(() => Criteria, criteria => criteria.ruleRef)
    @JoinTable()
    criteriaRef: Criteria[]

}