import { OmitType, PartialType } from '@nestjs/mapped-types/';
import { IsArray, IsDate, IsNumber, IsString } from 'class-validator';
import { Exclude, Expose } from 'class-transformer';
import { RecomendationDTO } from './recomendation.dto';
import { AreaDTO } from './area.dto';
import { CriteriaDTO } from './criteria.dto';

@Exclude()
export class RuleDTO {
    @Expose()
    @IsNumber()
    id: number;

    @Expose()
    @IsString()
    code: string;

    @Expose()
    @IsDate()
    createdAt: Date;

    @Expose()
    @IsNumber()
    @IsArray()
    recomendationIds:number[];

    @Expose()
    @IsArray()
    recomendations: RecomendationDTO[];

    @Expose()
    @IsNumber()
    @IsArray()
    aouIds: number[];

    @Expose()
    @IsArray()
    aous: AreaDTO[];

    @Expose()
    @IsNumber()
    @IsArray()
    criteriaId: number[];

    @Expose()
    @IsArray()
    criteria: CriteriaDTO[];
}
export class CreateRuleDTO extends OmitType(RuleDTO, ['id','criteria','aous','recomendations'] as const) {};
export class UpdateRuleDTO extends PartialType(CreateRuleDTO) {};
export class ResponseRuleDTO extends OmitType(RuleDTO, ['id','criteriaId','aouIds','recomendationIds'] as const) {};