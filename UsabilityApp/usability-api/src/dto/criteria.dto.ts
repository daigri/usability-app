import { OmitType, PartialType } from '@nestjs/mapped-types/';
import { IsArray, IsNumber, IsString } from 'class-validator';
import { Exclude, Expose } from 'class-transformer';
import { RuleDTO } from './rule.dto';
import { AreaDTO } from './area.dto';
import { ProblemDTO } from './problem.dto';

@Exclude()
export class CriteriaDTO {
    @Expose()
    @IsNumber()
    id: number;

    @Expose()
    @IsString()
    name: string;

    @Expose()
    @IsString()
    code: string;
    
    @Expose()
    @IsString()
    description: string;
    
    @Expose()
    @IsArray()
    ruleIds: number[];

    @Expose()
    rules: RuleDTO[];

    @Expose()
    @IsArray()
    areaIds: number[];

    @Expose()
    areas: AreaDTO[];

    @Expose()
    @IsArray()
    problemIds: number[];

    @Expose()
    problems: ProblemDTO[];
}
export class CreateCriteriaDTO extends OmitType(CriteriaDTO, ['id','problems','areas','rules'] as const) {};
export class UpdateCriteriaDTO extends PartialType(CriteriaDTO) {};
export class ResponseCriteriaDTO  extends OmitType(CriteriaDTO, ['problemIds','areaIds','ruleIds'] as const) {};
