import { OmitType, PartialType, PickType } from '@nestjs/mapped-types/';
import { IsArray, IsNumber, IsString } from 'class-validator';
import { Exclude, Expose } from 'class-transformer';
import { ProblemDTO } from './problem.dto';
import { RuleDTO } from './rule.dto';

@Exclude()
export class RecomendationDTO {
    @Expose()
    @IsNumber()
    id: number;

    @Expose()
    @IsString()
    recomendation: string;

    @Expose()
    @IsString()
    code: string;

    @Expose()
    @IsString()
    comment: string;

    @Expose()
    @IsArray()
    ruleRef: RuleDTO[];

    @Expose()
    @IsArray()
    ruleIds: number[];

    @Expose()
    @IsArray()
    problemIds: number[];

    @Expose()
    @IsArray()
    problemRef: ProblemDTO[];
}
export class CreateRecomendationDTO extends OmitType(RecomendationDTO, ['id','problemRef','ruleRef'] as const) {};
export class UpdateRecomendationDTO extends PartialType(CreateRecomendationDTO) {};
export class ResponseRecomendationDTO extends OmitType(RecomendationDTO, ['id','problemIds','ruleIds'] as const) {};