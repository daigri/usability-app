import { OmitType, PartialType, PickType } from '@nestjs/mapped-types/';
import { IsArray, IsNumber, IsString } from 'class-validator';
import { Exclude, Expose } from 'class-transformer';
import { RecomendationDTO } from './recomendation.dto';
import { CriteriaDTO } from './criteria.dto';

@Exclude()
export class ProblemDTO {
    @Expose()
    @IsNumber()
    id: number;

    @Expose()
    @IsString()
    name: string;

    @Expose()
    @IsString()
    code: string;

    @Expose()
    @IsString()
    comment: string;

    @Expose()
    @IsArray()
    recomendations: RecomendationDTO[];
    
    @Expose()
    @IsArray()
    recomendationIds: number[];

    @Expose()
    @IsArray()
    criteriaIds: number[];

    @Expose()
    @IsArray()
    criteria: CriteriaDTO[];
}
export class CreateProblemDTO extends OmitType(ProblemDTO, ['id','criteria','recomendations'] as const) {};
export class UpdateProblemDTO extends PartialType(ProblemDTO) {};
export class ResponseProblemDTO extends OmitType(ProblemDTO, ['criteriaIds','recomendationIds'] as const) {};
