import { OmitType, PartialType } from '@nestjs/mapped-types/';
import { IsArray, IsNumber, IsString } from 'class-validator';
import { Exclude, Expose } from 'class-transformer';
import { CriteriaDTO } from './criteria.dto';

@Exclude()
export class AreaDTO {
    @Expose()
    @IsNumber()
    id: number;

    @Expose()
    @IsString()
    name: string;

    @Expose()
    @IsString()
    code: string;

    @Expose()
    @IsString()
    description: string;

    @Expose()
    @IsArray()
    criteriaIds: number[];
    
    @Expose()
    criteria: CriteriaDTO[];
}
export class CreateAreaDTO extends OmitType(AreaDTO, ['id','criteria'] as const) {};
export class UpdateAreaDTO extends PartialType(AreaDTO) {};
export class ResponseAreaDTO extends OmitType(AreaDTO, ['criteriaIds'] as const) {};
