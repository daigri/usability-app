import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { DatabaseModule } from './database/database.module';
import { RecomendationModule } from './recomendation/recomendation.module';
import { CriteriaService } from './criteria/criteria.service';
import { ProblemService } from './problem/problem.service';
import { RuleService } from './rule/rule.service';
import { AreaService } from './area/area.service';
import { AreaController } from './area/area.controller';
import { CriteriaController } from './criteria/criteria.controller';
import { RuleController } from './rule/rule.controller';
import { ProblemController } from './problem/problem.controller';
import { RuleModule } from './rule/rule.module';
import { CriteriaModule } from './criteria/criteria.module';
import { ProblemModule } from './problem/problem.module';
import { AreaModule } from './area/area.module';

@Module({
  imports: [DatabaseModule, RecomendationModule, RuleModule, CriteriaModule, ProblemModule, AreaModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
