import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Rule } from 'src/entity/Rule';
import { RuleController } from './rule.controller';
import { RuleService } from './rule.service';

@Module({
    controllers: [RuleController],
    providers: [RuleService],
    imports: [TypeOrmModule.forFeature([Rule])]
})
export class RuleModule {}
