import { Body, Controller, Delete, Get, Param, Post } from "@nestjs/common";
import { CreateRuleDTO, ResponseRuleDTO } from "src/dto/rule.dto";
import { RuleService } from "./rule.service";

@Controller('rule')
export class RuleController {
    constructor (private readonly ruleService: RuleService){};

    @Get()
    async getAll():Promise<ResponseRuleDTO[]> {
        return await this.ruleService.getAll();
    }

    @Get('/:id')
    async one(@Param('id') id: number) {
        return await this.ruleService.get(id);
    }

    @Post()
    async save(@Body() area: CreateRuleDTO) {
        return await this.ruleService.create(area);
    }

    @Delete('/:id')
    async remove(@Param('id') id: number) {
       return await this.ruleService.delete(id);
    }
}
