import { HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { plainToClass } from 'class-transformer';
import { CreateRecomendationDTO, ResponseRecomendationDTO } from 'src/dto/recomendation.dto';
import { CreateRuleDTO, ResponseRuleDTO } from 'src/dto/rule.dto';
import { Recomendation } from 'src/entity/Recomendation';
import { Rule } from 'src/entity/Rule';
import { Repository } from 'typeorm';

@Injectable()
export class RuleService {
    constructor(@InjectRepository(Rule) private RuleRepository:Repository<Rule>){}

    public async create (dto: CreateRuleDTO): Promise<ResponseRuleDTO>{
       // var recomendationJson = instanceToPlain(CreateRecomendationDTO, { excludeExtraneousValues: true });
        var rule = plainToClass(Rule, dto);
        this.RuleRepository.save(rule);
        return plainToClass(ResponseRuleDTO,rule);
    }

    public async get (id: number): Promise<ResponseRuleDTO>{
        var rule = this.RuleRepository.findOne(id);
        var resp = plainToClass(ResponseRuleDTO, rule);
        return resp;
    }

    public async getAll (): Promise<ResponseRuleDTO[]>{
        var rule = await this.RuleRepository.find()
        var resp = rule.map(x => plainToClass(ResponseRuleDTO,x));
        return  resp;
    }

    public async delete (id: number): Promise<HttpStatus>{
        var rule = await this.RuleRepository.findOne(id);
        var resp = this.RuleRepository.remove(rule);
        return HttpStatus.ACCEPTED;
    }

    /*public async update (dto: UpdateRecomendationDTO): Promise<ResponseRecomendationDTO>{
        var recomendation = this.recomendationRepository.findOne(dto.id);
        recomendation
        return plainToClass(ResponseRecomendationDTO,recomendation);
    }*/
}
