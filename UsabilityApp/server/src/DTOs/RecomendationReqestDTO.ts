import { IsNumber, IsString } from 'class-validator';
import { Exclude, Expose } from 'class-transformer';

@Exclude()
export class RecomendationReqestDTO {
    @Expose()
    @IsNumber()
    id: number;

    @Expose()
    @IsString()
    recomendation: string;

    @Expose()
    @IsString()
    code: string;

    @Expose()
    @IsString()
    comment: string;
}
