import {getRepository} from "typeorm";
import { Body, Delete, Get, JsonController, Param, Post } from "routing-controllers";
import { Problem } from "../entity/Problem";

@JsonController()
export class ProblemController {

    private ConditionRepository = getRepository(Problem);

    @Get('/Problem')
    async all() {
        return this.ConditionRepository.find();
    }

    @Get('/Problem/:id')
    async one(@Param('id') id: number) {
        return this.ConditionRepository.findOne(id);
    }

    @Post('/Problem')
    async save(@Body() problem: Problem) {
        return this.ConditionRepository.save(problem);
    }

    @Delete('/Problem/:id')
    async remove(@Param('id') id: number) {
        let entity = await this.ConditionRepository.findOne(id);
        await this.ConditionRepository.remove(entity);
    }

}