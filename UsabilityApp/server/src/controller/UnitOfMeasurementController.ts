import {getRepository} from "typeorm";
import { Body, Delete, Get, JsonController, Param, Post } from "routing-controllers";
import { UnitOfMeasurement } from "../entity/UnitOfMeasurement";

@JsonController()
export class UnitOfMeasurementController {

    private ConditionRepository = getRepository(UnitOfMeasurement);

    @Get('/UnitOfMeasurement')
    async all() {
        return this.ConditionRepository.find();
    }

    @Get('/UnitOfMeasurement/:id')
    async one(@Param('id') id: number) {
        return this.ConditionRepository.findOne(id);
    }

    @Post('/Rule')
    async save(@Body() uom: UnitOfMeasurement) {
        return this.ConditionRepository.save(uom);
    }

    @Delete('/Rule/:id')
    async remove(@Param('id') id: number) {
        let entity = await this.ConditionRepository.findOne(id);
        await this.ConditionRepository.remove(entity);
    }

}