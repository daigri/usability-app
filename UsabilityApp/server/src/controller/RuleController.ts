import {getRepository} from "typeorm";
import { Body, Delete, Get, JsonController, Param, Post } from "routing-controllers";
import { Rule } from "../entity/Rule";

@JsonController()
export class RuleController {

    private ConditionRepository = getRepository(Rule);

    @Get('/Rule')
    async all() {
        return this.ConditionRepository.find();
    }

    @Get('/Rule/:id')
    async one(@Param('id') id: number) {
        return this.ConditionRepository.findOne(id);
    }

    @Post('/Rule')
    async save(@Body() rule: Rule) {
        return this.ConditionRepository.save(rule);
    }

    @Delete('/Rule/:id')
    async remove(@Param('id') id: number) {
        let entity = await this.ConditionRepository.findOne(id);
        await this.ConditionRepository.remove(entity);
    }

}