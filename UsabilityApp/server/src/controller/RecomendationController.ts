import {getRepository} from "typeorm";
import { Body, Delete, Get, JsonController, Param, Post } from "routing-controllers";
import { Recomendation } from "../entity/Recomendation";

@JsonController()
export class RecomendationController {

    private ConditionRepository = getRepository(Recomendation);

    @Get('/Recomendation')
    async all() {
        return this.ConditionRepository.find();
    }

    @Get('/Recomendation/:id')
    async one(@Param('id') id: number) {
        return this.ConditionRepository.findOne(id);
    }

    @Post('/Recomendation')
    async save(@Body() recomendation: Recomendation) {
        return this.ConditionRepository.save(recomendation);
    }

    @Delete('/Recomendation/:id')
    async remove(@Param('id') id: number) {
        let entity = await this.ConditionRepository.findOne(id);
        await this.ConditionRepository.remove(entity);
    }

}