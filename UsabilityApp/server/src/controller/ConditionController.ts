import {getRepository} from "typeorm";
import { Body, Delete, Get, JsonController, Param, Post } from "routing-controllers";
import { Condition } from "../entity/Condition";

@JsonController()
export class ConditionController {

    private ConditionRepository = getRepository(Condition);

    @Get('/Condition')
    async all() {
        return this.ConditionRepository.find();
    }

    @Get('/Condition/:id')
    async one(@Param('id') id: number) {
        return this.ConditionRepository.findOne(id);
    }

    @Post('/Condition')
    async save(@Body() condition: Condition) {
        return this.ConditionRepository.save(condition);
    }

    @Delete('/Condition/:id')
    async remove(@Param('id') id: number) {
        let entity = await this.ConditionRepository.findOne(id);
        await this.ConditionRepository.remove(entity);
    }

}