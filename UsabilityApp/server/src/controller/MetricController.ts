import {getRepository} from "typeorm";
import { Body, Delete, Get, JsonController, Param, Post } from "routing-controllers";
import { Metric } from "../entity/Metric";

@JsonController()
export class MetricController {

    private ConditionRepository = getRepository(Metric);

    @Get('/Metric')
    async all() {
        return this.ConditionRepository.find();
    }

    @Get('/Metric/:id')
    async one(@Param('id') id: number) {
        return this.ConditionRepository.findOne(id);
    }

    @Post('/Metric')
    async save(@Body() metric: Metric) {
        return this.ConditionRepository.save(metric);
    }

    @Delete('/Metric/:id')
    async remove(@Param('id') id: number) {
        let entity = await this.ConditionRepository.findOne(id);
        await this.ConditionRepository.remove(entity);
    }

}