import {getRepository} from "typeorm";
import { Body, Delete, Get, JsonController, Param, Post } from "routing-controllers";
import { Criteria } from "../entity/Criteria";

@JsonController()
export class CriteriaController {

    private ConditionRepository = getRepository(Criteria);

    @Get('/Criteria')
    async all() {
        return this.ConditionRepository.find();
    }

    @Get('/Criteria/:id')
    async one(@Param('id') id: number) {
        return this.ConditionRepository.findOne(id);
    }

    @Post('/Criteria')
    async save(@Body() criteria: Criteria) {
        return this.ConditionRepository.save(criteria);
    }

    @Delete('/Criteria/:id')
    async remove(@Param('id') id: number) {
        let entity = await this.ConditionRepository.findOne(id);
        await this.ConditionRepository.remove(entity);
    }

}