import {getRepository} from "typeorm";
import {AreaOfUse} from "../entity/AreaOfUse";
import { Body, Delete, Get, JsonController, Param, Post } from "routing-controllers";

@JsonController()
export class AreOfUseController {

    private areOfUseRepository = getRepository(AreaOfUse);

    @Get('/AreOfUse')
    async all() {
        return this.areOfUseRepository.find();
    }

    @Get('/AreOfUse/:id')
    async one(@Param('id') id: number) {
        return this.areOfUseRepository.findOne(id);
    }

    @Post('/AreOfUse')
    async save(@Body() aou: AreaOfUse) {
        return this.areOfUseRepository.save(aou);
    }

    @Delete('/AreOfUse/:id')
    async remove(@Param('id') id: number) {
        let entity = await this.areOfUseRepository.findOne(id);
        await this.areOfUseRepository.remove(entity);
    }

}