import "reflect-metadata";
import {createConnection} from "typeorm";
import * as express from "express";
import 'reflect-metadata';
import {Request, Response} from "express";
import { createExpressServer } from 'routing-controllers';
import {Routes} from "./routes";
import { TryDBConnect } from "./helpers/db";
import { AreOfUseController } from "./controller/AreOfUseController";
import { ConditionController } from "./controller/ConditionController";
import { CriteriaController } from "./controller/CriteriaController";
import { MetricController } from "./controller/MetricController";
import { ProblemController } from "./controller/ProblemController";
import { RecomendationController } from "./controller/RecomendationController";
import { RuleController } from "./controller/RuleController";
import { UnitOfMeasurementController } from "./controller/UnitOfMeasurementController";
import cors = require("cors");

createConnection().then(async connection => {

    // create express app
    const app = createExpressServer({controllers: [
        AreOfUseController,
        ConditionController,
        CriteriaController,
        MetricController,
        ProblemController,
        RecomendationController,
        RuleController,
        UnitOfMeasurementController 
    ]});
    app.use(cors());
    app.use(express.json());

    app.use(async (req: Request, res: Response, next) => {
        await TryDBConnect(() => {
          res.json({
            error: 'Database connection error, please try again later',
          });
        }, next);
      });

    // register express routes from defined application routes
    Routes.forEach(route => {
        (app as any)[route.method](route.route, (req: Request, res: Response, next: Function) => {
            const result = (new (route.controller as any))[route.action](req, res, next);
            if (result instanceof Promise) {
                result.then(result => result !== null && result !== undefined ? res.send(result) : undefined);

            } else if (result !== null && result !== undefined) {
                res.json(result);
            }
        });
    });

    // setup express app here
    // ...

    // start express server
    app.listen(3000);


    console.log("Express server has started on port 3000. Open http://localhost:3000/users to see results");

}).catch(error => console.log(error));
