import {Entity, PrimaryGeneratedColumn, Column, ManyToMany} from "typeorm";
import { Criteria } from "./Criteria";
import { Problem } from "./Problem";
import { Rule } from "./Rule";

@Entity()
export class AreaOfUse {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    code: string;

    @Column()
    description: string;
    
    @ManyToMany(() => Rule, rule => rule.areOfUseRef)
    ruleRef: AreaOfUse[];
    
    @ManyToMany(() => Criteria, criteria => criteria.areOfUseRef)
    criteriaRef: Criteria[];
}