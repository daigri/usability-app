import {Entity, PrimaryGeneratedColumn, Column, ManyToMany} from "typeorm";
import { AreaOfUse } from "./AreaOfUse";
import { Problem } from "./Problem";
import { Rule } from "./Rule";

@Entity()
export class Criteria {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    code: string;

    @Column()
    description: string;
    
    @ManyToMany(() => Problem, problem => problem.recomendationRef)
    problemRef: Problem[];
    
    @ManyToMany(() => AreaOfUse, aou => aou.criteriaRef)
    areOfUseRef: Criteria[];

    @ManyToMany(() => Rule, rule => rule.areOfUseRef)
    ruleRef: Rule[];
}