import {Entity, PrimaryGeneratedColumn, Column, ManyToMany, JoinTable} from "typeorm";
import { Recomendation } from "./Recomendation";

@Entity()
export class Problem {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    recomendation: string;

    @Column()
    code: string;

    @Column()
    comment: string;

    @ManyToMany(() => Recomendation, recomendation => recomendation.problemRef)
    @JoinTable()
    recomendationRef: Recomendation[]

}